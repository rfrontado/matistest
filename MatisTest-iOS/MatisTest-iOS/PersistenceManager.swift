//
//  PersistenceManager.swift
//  MatisTest-iOS
//
//  Created by Roberto Frontado on 8/18/15.
//  Copyright (c) 2015 Roberto Frontado. All rights reserved.
//

import UIKit
import Realm

class PersistenceManager: NSObject {
   
    class func savePointsOfInterest(pointsOfInterest: RLMArray) {
        
        for item in pointsOfInterest
        {
            RLMRealm.defaultRealm().beginWriteTransaction()
            PointOfInterest.createOrUpdateInDefaultRealmWithObject(item)
            RLMRealm.defaultRealm().commitWriteTransaction()
        }
    }
    
    class func loadPointsOfInterest() -> RLMArray {
        var pointsOfInterest = RLMArray(objectClassName: PointOfInterest.className())

        let result = PointOfInterest.allObjects()
        pointsOfInterest.addObjects(result)
        
        return pointsOfInterest
    }
    
    class func getFilteredPointsOfInterest(filter: String) -> RLMArray {
        var pointsOfInterest = RLMArray(objectClassName: PointOfInterest.className())
        
        let result = PointOfInterest.allObjects()
        let filteredResults = result.objectsWhere("title BEGINSWITH[c] '\(filter)'")
        pointsOfInterest.addObjects(filteredResults)
        
        return pointsOfInterest
    }
    
}
