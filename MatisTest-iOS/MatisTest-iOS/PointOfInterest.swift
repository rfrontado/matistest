//
//  PointOfInterest.swift
//  MatisTest-iOS
//
//  Created by Roberto Frontado on 8/17/15.
//  Copyright (c) 2015 Roberto Frontado. All rights reserved.
//

import UIKit
import Alamofire
import Realm

class PointOfInterest: RLMObject {
   
    dynamic var pointID = ""
    dynamic var title: String = ""
    dynamic var latitude: String = ""
    dynamic var longitude: String = ""
    dynamic var pointDescription: String = ""
    dynamic var phone: String = ""
    dynamic var address: String = ""
    dynamic var transport: String = ""
    dynamic var email: String = ""
    
    override init() {
        super.init()
    }
    
    override class func primaryKey() -> String! {
        return "pointID"
    }
    
    class func getPointOfInterestDetail(pointID: String, successBlock: (pointOfInterest: PointOfInterest) -> Void, failureBlock: (error: NSError) -> Void) {
        
        let url = kPOINTS_OF_INTEREST_DETAIL + pointID
        
        Alamofire.request(.GET, url, parameters: nil, encoding: .JSON).responseJSON { (request, response, json, error) -> Void in
            
            if error != nil
            {
                failureBlock(error: error!)
                return
            }
            
            if let title = json?.objectForKey("title") as? String,
                address = json?.objectForKey("address") as? String,
                transport = json?.objectForKey("transport") as? String,
                email = json?.objectForKey("email") as? String,
                geocoordinates = json?.objectForKey("geocoordinates")  as? String,
                pointDescription = json?.objectForKey("description") as? String,
                phone = json?.objectForKey("phone") as? String
            {
                
                let latitude = geocoordinates.componentsSeparatedByString(",")[0]
                let longitude = geocoordinates.componentsSeparatedByString(",")[1]
            
                let pointOfInterest = PointOfInterest()
                pointOfInterest.pointID = pointID
                pointOfInterest.title = title
                pointOfInterest.address = address
                pointOfInterest.transport = transport
                pointOfInterest.email = email
                pointOfInterest.latitude = latitude
                pointOfInterest.longitude = longitude
                pointOfInterest.pointDescription = pointDescription
                pointOfInterest.phone = phone
                
                successBlock(pointOfInterest: pointOfInterest)
            }
            else
            {
                let error = NSError(domain: "ERROR_DOMAIN", code: 01, userInfo: ["Error": "Failed parsing json"])
                failureBlock(error: error)
                return
            }
            
        }
        
    }

    class func getPointsOfInterest(successBlock: (pointsOfInterest: RLMArray) -> Void, failureBlock: (error: NSError) -> Void) {
        
        Alamofire.request(.GET, kPOINTS_OF_INTEREST_LIST, parameters: nil, encoding: .JSON).responseJSON { (request, response, json, error) -> Void in
            
            if error != nil
            {
                failureBlock(error: error!)
                return
            }
            
            if let list = json?.objectForKey("list") as? [[String: AnyObject]]
            {
                
                var pointsOfInterest = RLMArray(objectClassName: PointOfInterest.className())
                
                for item in list
                {
                    if let pointID = item["id"] as? String,
                        title = item["title"] as? String,
                        geocoordinates = item["geocoordinates"] as? String
                    {
                        let latitude = geocoordinates.componentsSeparatedByString(",")[0]
                        let longitude = geocoordinates.componentsSeparatedByString(",")[1]
                        
                        var pointOfInterest = PointOfInterest()
                        pointOfInterest.pointID = pointID
                        pointOfInterest.title = title
                        pointOfInterest.latitude = latitude
                        pointOfInterest.longitude = longitude
                        
                        pointsOfInterest.addObject(pointOfInterest)
                    }
                    else
                    {
                        let error = NSError(domain: "ERROR_DOMAIN", code: 01, userInfo: ["Error": "Failed parsing json"])
                        failureBlock(error: error)
                        return
                    }
                    
                }
                PersistenceManager.savePointsOfInterest(pointsOfInterest)
                successBlock(pointsOfInterest: pointsOfInterest)
            }
            else
            {
                let error = NSError(domain: "ERROR_DOMAIN", code: 01, userInfo: ["Error": "Failed parsing json"])
                failureBlock(error: error)
                return
            }
            
        }
        
    }
    
}


