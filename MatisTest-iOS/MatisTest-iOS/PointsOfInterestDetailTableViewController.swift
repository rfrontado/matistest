//
//  PointsOfInterestDetailTableViewController.swift
//  MatisTest-iOS
//
//  Created by Roberto Frontado on 8/18/15.
//  Copyright (c) 2015 Roberto Frontado. All rights reserved.
//

import UIKit
import GoogleMaps

class PointsOfInterestDetailTableViewController: UITableViewController {
    
    @IBOutlet weak var mapContainer: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var transportLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var pointOfInterest: PointOfInterest!
    var mapView: GMSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        
        if pointOfInterest != nil
        {
            PointOfInterest.getPointOfInterestDetail(pointOfInterest.pointID, successBlock: { (pointOfInterest) -> Void in
                self.pointOfInterest = pointOfInterest
                
                self.titleLabel.text = self.validateText(pointOfInterest.title)
                self.emailLabel.text = self.validateText(pointOfInterest.email)
                self.phoneLabel.text = self.validateText(pointOfInterest.phone)
                self.addressLabel.text = self.validateText(pointOfInterest.address)
                self.transportLabel.text = self.validateText(pointOfInterest.transport)
                self.descriptionLabel.text = self.validateText(pointOfInterest.pointDescription)
                self.addMap(pointOfInterest.latitude, longitude: pointOfInterest.longitude)
                
                self.tableView.reloadData()
                
                }) { (error) -> Void in
                    PopUpUtils.simplePopUp(self, message: error.description)
            }
        }
        
    }
    
    // MARK: - Private Methods
    func addMap(latitude: String, longitude: String) {
        
        var marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(latitude.toDouble(), longitude.toDouble())
        
        let camera = GMSCameraPosition.cameraWithLatitude(latitude.toDouble(),
            longitude: longitude.toDouble(), zoom: 13)
        let mapFrame = CGRectMake(0, 0, self.view.frame.width, mapContainer.frame.height)
        mapView = GMSMapView.mapWithFrame(mapFrame, camera: camera)
        
        mapView.myLocationEnabled = true
        mapView.userInteractionEnabled = false
        mapContainer.addSubview(mapView)
        
        marker.map = mapView
    }
    
    func validateText(text: String) -> String{
        if text == "null" || text == "undefined" || text == ""
        {
            return "No disponible"
        }
        return text
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

//    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//        // #warning Potentially incomplete method implementation.
//        // Return the number of sections.
//        return 0
//    }
//
//    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete method implementation.
//        // Return the number of rows in the section.
//        return 0
//    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! UITableViewCell

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
