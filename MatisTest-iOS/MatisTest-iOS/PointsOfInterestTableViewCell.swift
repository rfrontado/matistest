//
//  PointsOfInterestTableViewCell.swift
//  MatisTest-iOS
//
//  Created by Roberto Frontado on 8/17/15.
//  Copyright (c) 2015 Roberto Frontado. All rights reserved.
//

import UIKit

class PointsOfInterestTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
