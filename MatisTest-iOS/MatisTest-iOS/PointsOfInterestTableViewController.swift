//
//  PointsOfInterestTableViewController.swift
//  MatisTest-iOS
//
//  Created by Roberto Frontado on 8/17/15.
//  Copyright (c) 2015 Roberto Frontado. All rights reserved.
//

import UIKit
import Realm

class PointsOfInterestTableViewController: UITableViewController, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    
    var pointsOfInterest = RLMArray(objectClassName: PointOfInterest.className())
    var selectedPointOfInterest: PointOfInterest!
    var lastCellShown = 0
    
    let animationDuration = 0.3
    let cellIdentifier = "cell"
    let segueIdentifier = "Detail"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        
        //Load the local data
        pointsOfInterest = PersistenceManager.loadPointsOfInterest()
        
        //Ask for new data and update the local data
        PointOfInterest.getPointsOfInterest({ (pointsOfInterest) -> Void in
            self.pointsOfInterest = pointsOfInterest
            self.tableView.reloadData()
        }, failureBlock: { (error) -> Void in
            PopUpUtils.simplePopUp(self, message: error.description)
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        super.touchesBegan(touches, withEvent: event)
        hideKeyboard()
    }
    
    // MARK: - Private methods
    
    func hideKeyboard(){
        searchBar.resignFirstResponder()
    }
    
    // MARK: - UIScrollView Delegate
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        hideKeyboard()
    }
    
    // MARK: - Table View Data Source

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return Int(pointsOfInterest.count)
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! PointsOfInterestTableViewCell
        
        let pointOfInterest = pointsOfInterest[UInt(indexPath.row)] as! PointOfInterest
        cell.titleLabel.text = pointOfInterest.title

        return cell
    }

    // MARK: - Table view delegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        hideKeyboard()
        selectedPointOfInterest = pointsOfInterest[UInt(indexPath.row)] as! PointOfInterest
        performSegueWithIdentifier(segueIdentifier, sender: self)
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row >= lastCellShown
        {
            lastCellShown = indexPath.row
            cell.frame.origin = CGPointMake(-cell.frame.width, cell.frame.origin.y)
            UIView.animateWithDuration(animationDuration, animations: { () -> Void in
                cell.frame.origin = CGPointMake(0, cell.frame.origin.y)
            })
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        if segue.identifier == segueIdentifier
        {
            if let dest = segue.destinationViewController as? PointsOfInterestDetailTableViewController
            {
                dest.pointOfInterest = selectedPointOfInterest
            }
        }
    }
    
    // MARK: - Search Bar Delegate

    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        //Filter the data
        pointsOfInterest = PersistenceManager.getFilteredPointsOfInterest(searchText)
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        hideKeyboard()
    }

}
