
//
//  PopUpUtils.swift
//  MatisTest-iOS
//
//  Created by Roberto Frontado on 8/17/15.
//  Copyright (c) 2015 Roberto Frontado. All rights reserved.
//

import UIKit

class PopUpUtils: NSObject {

    class func simplePopUp(viewController: UIViewController, message: String) {
        
        var alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        viewController.presentViewController(alert, animated: true, completion: nil)
    }
    
}