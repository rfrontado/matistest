//
//  Strings+Utils.swift
//  MatisTest-iOS
//
//  Created by Roberto Frontado on 8/18/15.
//  Copyright (c) 2015 Roberto Frontado. All rights reserved.
//

import Foundation

extension String {
    
    func toDouble() -> Double {
        return (self as NSString).doubleValue
    }
}